package week2abc;//123

import java.util.concurrent.*;
import javafx.application.*;
import javafx.beans.binding.*;
import javafx.scene.*;
import javafx.scene.web.*;
import javafx.stage.*;

/**
 * A helper class that shows a JavaFX WebView in a window.
 * HTML can be shown directly by the show method.
 * A URL can be opened with the open method.
 * <p>
 * This class should only be called from the main thread.adfklasjdflkajsdfasdfasdf
 */
public class MiniBrowser extends Application {

    private static final Semaphore semaphore = new Semaphore(0);
    private volatile static Stage stage;
    private static WebView browser;

    /**
     * Show HTML in a window.
     * This method blocks until the user closes the window.
     * @param html the HTML source of the web page to render
     */
    public static void show(String html) {
        System.out.println("Showing HTML: " + html);
        startPlatform();
        Platform.runLater(() -> {
            browser.getEngine().loadContent(html);
            stage.show();
            // blah
        });
        semaphore.acquireUninterruptibly();
    }

    /**
     * Open a URL in a window.
     * This method blocks until the user closes the window.
     * @param url the address of the web page to open
     */
    public static void open(String url) {
        System.out.println("Opening: " + url);
        startPlatform();
        Platform.runLater(() -> {
            browser.getEngine().load(url);
            stage.show();
        });
        semaphore.acquireUninterruptibly();
    }

    /**
     * Starts the JavaFX platform.
     * The current thread is blocked until JavaFX has started.
     * When the current thread ends, this will be detected and JavaFX will be shut down.
     */
    private static void startPlatform() {
        if (stage == null) {
            // Start JavaFX and wait for the HTMLViewer to start
            new Thread(() -> Application.launch(MiniBrowser.class)).start();
            semaphore.acquireUninterruptibly();

            // Start a thread to monitor the Main thread, shutting down when main exits
            final Thread main = Thread.currentThread();
            new Thread(() -> {
                try {
                    main.join();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
                Platform.exit();
            }).start();
        }
    }

    /**
     * The JavaFX start method.
     * Once called, the semaphore is released to notify the waiting thread that JavaFX is ready.
     * @param stage
     * @throws Exception 
     */
    @Override
    public void start(Stage stage) throws Exception {
        Platform.setImplicitExit(false);
        MiniBrowser.stage = stage;
        MiniBrowser.browser = new WebView();

        // Bind the Window title to the web view's title (if present)
        // or just "HTML Viewer" otherwise
        StringExpression title = browser.getEngine().titleProperty();
        StringExpression windowTitle = new When(title.isNull()).then("HTML Viewer").otherwise(title);
        stage.titleProperty().bind(windowTitle);
        stage.setScene(new Scene(browser));

        // Notify the blocking thread when the window is closed
        stage.setOnCloseRequest(e -> semaphore.release());

        // Immediately notify listeners waiting for JavaFX to start
        semaphore.release();
    }

}